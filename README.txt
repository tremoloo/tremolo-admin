Tremolo Admin
==============
This is the administrative heart of Tremolo. Allows to manage Tremolo user and site settings.

Key Features
------------
* Addition of commonly used roles on all sites that content needs to be managed and there's some kind of support from somebody else. Roles are:
 - writer
 - editor






[* Customized Content Editing Experience
* Custom Style for the Media Browser
* Creates Panopoly Administrative Section (admin/panopoly) for Panopoly Configuration
* Provides Pane Library, Layout Library, and Page Library Administrative Pages
* Packages Several Different Administrative Toolbar Options (Admin Menu, Admin, Navbar, Toolbar)
}