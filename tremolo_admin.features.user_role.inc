<?php
/**
 * @file
 * tremolo_admin.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function tremolo_admin_user_default_roles() {
  $roles = array();

  // Exported role: editor.
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => 3,
  );

  // Exported role: writer.
  $roles['writer'] = array(
    'name' => 'writer',
    'weight' => 4,
  );

  return $roles;
}
